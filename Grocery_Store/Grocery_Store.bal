import ballerina/http;
import ballerinax/mongodb;
import ballerina/io;
import ballerinax/kafka;

public type Grocery record {
    int id;
    string name;
    float price;
    Store[] store;
    boolean isAvailable;
};

public type Order record {
    int id;
    string customerNumber;
    Request[] request;
    string address;
};

public type Request record {
    int productId;
    int quantity;
};

public type Store record {
    string name;
    int quantity;
};

//Mongo DB
mongodb:ConnectionConfig mongoConfig = {
        host: "localhost",
        port: 27017,
        options: {sslEnabled: false, serverSelectionTimeout: 5000}
    };

mongodb:Client mongoClient = checkpanic new (mongoConfig, "GroceryStore");

//Kafka
public type OrderProducerRecord record {|
    *kafka:AnydataProducerRecord;
    Order value;
    int key;
|};

kafka:Producer orderProducer = check new (kafka:DEFAULT_URL);

// @http:ServiceConfig {
//     auth: [
//         {
//             fileUserStoreConfig: {},
//             scopes: ["scope1"]
//         }
//     ]
// }
service /custerService on new http:Listener(9098) {

    resource function get GetGroceries() returns Grocery[]|error {

        stream<Grocery, error?> groceries = check mongoClient->find("Grocery", filter = {isAvailable: true});

        Grocery[] result = [];
        check groceries.forEach(function(Grocery grocery) {
            result.push(grocery);
        });

        return result;
    }

    resource function post orderGroceries(http:Caller caller, http:Request request) returns error? {
        http:Response response = new;
        json|error reqPayload = request.getJsonPayload();

        if (reqPayload is error) {
            response.statusCode = 400;
            response.setJsonPayload({"Message": "Invalid payload - Not a valid JSON payload"});
            var result = caller->respond(response);
            io:print(result);
        } else {

            //get full order
            Order reqOrder = check reqPayload.cloneWithType();

            int[] availableIds = reqOrder.request.'map(x => x.productId);

            //get all groceries
            stream<Grocery, error?> groceries = check mongoClient->find("Grocery", filter = {isAvailable: true});

            Grocery[] result = [];

            check groceries.forEach(function(Grocery grocery) {
                foreach int item in availableIds {
                    if (grocery.id == item) {
                        result.push(grocery);
                    }
                }
            });

            string Availability = "";

            foreach Grocery item in result {
                int sum = 0;
                foreach Store value in item.store {
                    sum += value.quantity;
                }

                foreach Request actual in reqOrder.request {
                    if (actual.productId == item.id && actual.quantity > sum) {
                        Availability = "Error";
                    }
                }
            }

            if (Availability == "Error") {
                response.statusCode = 400;
                response.setJsonPayload({"Message": "Available quantity of an item is less than what is ordered. adjust the quantity, cancel the item or cancel the order altogether"});
                var responseResu = caller->respond(response);
                io:print(responseResu);
            } else {

                map<json> saveRequest = <map<json>>reqOrder.toJson();

                OrderProducerRecord producerRecord = {
                    key: 1,
                    topic: "grocery-store",
                    value: reqOrder
                };

                // Sends the message to the Kafka topic.
                check orderProducer->send(producerRecord);

                check mongoClient->insert(saveRequest, "Order");
                mongoClient->close();

                response.setJsonPayload({"Status": "Success"});
                var responseResult = caller->respond(response);
                io:print(responseResult);
            }

        }
    }
}

